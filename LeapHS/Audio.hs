module Audio
    (Music,
     initAudio,
     loadMusic,
     playMusic,
     playFile,
     stopMusic) where

import Control.Concurrent
import Data.Maybe (fromJust, isNothing)
import qualified Graphics.UI.SDL.Mixer.General as SDL.Mixer
import qualified Graphics.UI.SDL.Mixer.Channels as SDL.Mixer.Channels
import qualified Graphics.UI.SDL.Mixer.Music as SDL.Mixer.Music
import qualified Graphics.UI.SDL.Mixer.Types as SDL.Mixer.Types
import qualified Graphics.UI.SDL.Mixer.Samples as SDL.Mixer.Samples

type Music = SDL.Mixer.Types.Music

initAudio :: IO ()
initAudio = SDL.Mixer.openAudio 44100 SDL.Mixer.AudioS16Sys 2 4096

loadMusic :: String -> IO Music
loadMusic = SDL.Mixer.Music.loadMUS

playMusic :: Music -> IO ()
playMusic m = do SDL.Mixer.Music.setMusicVolume 50
                 SDL.Mixer.Music.playMusic m (-1)

stopMusic :: IO ()
stopMusic = SDL.Mixer.Music.haltMusic

playFile :: String -> Int -> IO ()
playFile f t = do
  wav <- SDL.Mixer.Samples.tryLoadWAV f
  if isNothing wav
   then putStrLn $ "Could not load " ++ f
   else do _ <- forkIO $ runInBoundThread $ 
                     do _ <- SDL.Mixer.Channels.playChannel (-1) (fromJust wav) 0
                        threadDelay (t * 1000)
                        return ()
           return ()
